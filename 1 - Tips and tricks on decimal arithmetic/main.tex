\documentclass[10pt]{article} 

\usepackage{amsmath, amssymb, fullpage}

\begin{document}
\title{Tips and Tricks on Decimal Arithmetic}
\maketitle
\setlength{\parindent}{0in}
\section{Important Facts}
When given a mathematical expression to evaluate its value, how do you judge if it is a difficult one or not? Most people might think that the longer the given expression, the harder it is. This might be true to some extend, but sometimes, a longer expression \emph{might} be much easier to evaluate. For instance, of the following expressions, the former is, by far, the shorter one, but the second expression is much easier to calculate, as we shall see in Example 4 later on.
\[2012+201.2+20.12+2.012+0.2012\]
\[2012+2012\times0.1+2012\times0.01+2012\times0.001\]
The spirit behind this chapter is \emph{not} to shorten an expression, but to find a path that is much easier for us to arrive at the answer.
\subsection*{The Use of Laws of Arithmetic}
\begin{enumerate}
\item Commutative Law for Addition: $a+b=b+a$
\item Associative Law for Addition: $(a+b)+c=a+(b+c)$
\item Commutative Law for Multiplication: $ab=ba$
\item Associative Law for Multiplication: $(ab)c=a(bc)$
\item Distributive Law: $a(b+c)=ab+ac$
\end{enumerate}
It might be useful to spend some time to analyse and rearrange terms while applying the arithmetic laws to simplify the original expression. Also, when using the distributive law, take note of the sign (positive or negative) for each term and multiply accordingly:
\begin{enumerate}
\item $(+)\times(+)=(+)$
\item $(-)\times(+)=(-)$
\item $(+)\times(-)=(-)$
\item $(-)\times(-)=(+)$
\end{enumerate}
These might seem non-intuitive, but each of these can be ``proven" by looking at the definition of multiplication. We know that
\[a\times b = \underbrace{a+a+a+\cdots+a}_b\]
Now, if we assume that both of $a$ and $b$ are positive, we can clearly see that $a+a+a+\cdots+a$ is positive, which agrees with the first point in the above list. When $a$ is negative, it is also clear that $a+a+a+\cdots+a$ is negative, which agrees with the next point. What about when $b$ is negative? Indeed, it is difficult to imagine ``add $a$ to itself for -5 times", but we can think for it from another perspective. $a\times b$ besides being interpreted as the above expression, can also be written as 
\[a\times b = \underbrace{b+b+b+\cdots+b}_a\]
which is very similar to the previous representation, and hence, the third point is true as well. Notice that for any negative integer $-n$, we can actually write $-n$ as $(-1)\times n$. In other words, we can write any negative integer as the product of $-1$ and that integer without the negative sign. We can use this knowledge and apply it on the Distributive Law. Let $a=-1$, we have
\[-(b+c)=-b-c\]
Also take note that when applying the Commutative to move the terms around, it is important to move the positive or negative sign in front as well. For example,
\[3-5+2=3+2-5\]
Notice that the negative sign moves together with 5.\\
Another fact that is useful to know is that equality can be read both way: given $a=b$, we can also say that $b=a$.

\subsection*{Rewriting a Decimal as a Product of an Integer and a Sub-multiple of Ten}
This method is especially useful when some of the terms in the expression can be written as the product of a \emph{same} integer and \emph{different} sub-multiple of ten. After that, the reverse of Distributive Law is often applied to collectively simplify the sub-multiples.

\subsection*{Rewriting an Integer as the Product of its Factor}
Another useful trick is to decompose an integer in an expression to its factors and apply some of the Laws of Arithmetic to simplify the given expression.  When using this trick, it is very helpful to be able quickly identify the fraction representation of a decimal, especially the reciprocal of powers of 2 and 5. For example
\[0.5 = \frac{1}{2}\]
\[0.25 = \frac{1}{4}\]
\[0.125 = \frac{1}{8}\]
\[\vdots\]
\[0.2 = \frac{1}{5}\]
\[0.04 = \frac{1}{25}\]
\[0.008 = \frac{1}{125}\]
\[\vdots\]

\section{Examples}
\subsection*{The Use of Arithmetic Laws}
\textbf{Example 1}\\
Compute
\[3.75-1.64+(10.25-0.36)\]
Comment: It can be easily observed that 3.75 and 10.25 sum up to be an integer, 1.64 and 0.36 also sum up to be an integer, but notice that both 1.64 and 0.36 are negative. So, this suggests the use of Distributive Law.\\
\textit{Solution:}
\begin{align*}
3.75-1.64+(10.25-0.36) &= 3.75-1.64+10.25-0.36\\
&=3.75+10.25-1.64-0.36\\
&=(3.75+10.25)-(1.64+0.36)\\
&=14-2\\
&=12
\end{align*}
As mentioned before, we can use the fact that equality can be read both ways. Here,
\[-a-b=-(a+b)\]

\textbf{Example 2}\\
Compute
\[4.82-7.76+8.8+1.18-2.24+10.2\]
Comment: Notice that the expression given can be paired such that each pair sums up to be an integer. To achieve such pairing, we need Commutative Law, that is, to rearrange the terms.\\
\textit{Solution:}
\begin{align*}
4.82-7.76+8.8+1.18-2.24+10.2 &= (4.82+1.18)-(7.76+2.24)+(8.8+10.2)\\
&= 6-10+19\\
&= 15
\end{align*}

\textbf{Example 3}\\
Compute
\[86.5\times 1.32+86.5\times 0.68-86.5\]
Comment: Clearly, the multiple occurrence of 86.5 is hinting the use of Distributive Law.\\
\textit{Solution:}
\begin{align*}
86.5\times 1.32 +86.5\times 0.68 -86.5 &= 86.5(1.32+0.68-1)\\
&= 86.5\times 1\\
&= 86.5
\end{align*}

\subsection*{Rewriting a Decimal as a Product of an Integer and a Sub-multiple of Ten}
\textbf{Example 4}\\
Compute
\[2012+201.2+20.12+2.012+0.2012\]
Comment: Each term in the expression can be written as 2012 and a sub-multiple of ten. Together with Distributive Law, the expression given can be simplified.\\
\textit{Solution:}
\begin{align*}
2012+201.2+20.12+2.012+0.2012 &= 2012 + 2012\times0.1 + 2012\times0.01 + 2012\times0.001 + 2012\times0.0001\\
&= 2012 \times (1+0.1+0.01+0.001+0.0001)\\
&= 2012 \times 1.1111\\
&= (2000+10+2)\times1.1111\\
&= 2000\times1.1111 + 10\times1.1111 + 2\times1.1111\\
&= 2222.2 + 11.111+ 2.2222\\
&= 2233.311 + 2.2222\\
&= 2235.5332
\end{align*}

\subsection*{Rewriting an Integer as the Product of its Factor}
\textbf{Example 5}\\
Compute
\[0.5\times0.125\times0.04\times 400\]
Comment: As mentioned before, 0.5, 0.125 and 0.04 are some of the ``special'' decimals. So, how special are they, and what is the use of 400 here?\\
\textit{Solution:}
\begin{align*}
0.5\times0.125\times0.04\times400 &= \frac{1}{2}\times\frac{1}{8}\times\frac{1}{25}\times400\\
&= \frac{1}{2}\times\frac{1}{8}\times\frac{1}{25}\times(2\times8\times25)\\
&= \frac{1}{2}\times2\times\frac{1}{8}\times8\times\frac{1}{25}\times25\\
&= 1
\end{align*}

\section{Exercises}
\begin{enumerate}
\item Compute $99\times 12.5 + 12.5$
\item Compute $1440\div 12 - 888\times 0.125$
\item Compute $1.28+2.33+3.46+5.72+6.67+7.54$
\item Compute $123456789\times 987654321\times (25-125\times0.2)$
\item Compute $2.34-(0.6+0.008\div 0.04)\times 0.3$
\item Compute $0.234\times 123+0.234+2012\times 0.234 +1.3\times 332\times 0.18$
\item Compute $2012\times 2-201.2\times 22.3 +20.12\times123$
\item Compute $(1.2\times 1.25 + 0.5)\div (0.25\times 8 - 0.008\times125)$
\item Compute $125.25+101.75\times0.28+0.72\times101.5+0.72\times0.25$
\item Compute $1.0+1.2+1.4+1.6+1.8+2.0+\cdots+9.6+9.8$
\item Compute $1234\times 314159\times 4321\times (4\times 0.25 - 0.008\times 125)$
\item Compute $0.0625\times 3.552\times4+0.8\times0.14$
\item Compute, up to 3 decimal places, $0.\dot{2}\dot{4}\dot{6}\dot{8}+3.141592\dot{6}+0.125$.
\item We write down $1, 2, 3, \cdots$ after $0.$ as shown below:
\[0.123456789101112131415\cdots\]
We can see that the 11th digit after the decimal point is 0, while the 15th digit is 2. What is the 2013th digit?
\end{enumerate}

\section{Answers to Exercises}
\begin{enumerate}
\item 1250
\item 9
\item 27
\item 0
\item 2.1
\item 577.512
\item 2012
\item 2
\item 227
\item 243
\item 0
\item 1
\item 3.513
\item 7
\end{enumerate}

\section{Solutions to Exercises}
\begin{enumerate}
\item \begin{align*}99\times 12.5+12.5 &= 12.5\times(99+1)\\&=12.5\times100\\&=1250\end{align*}
\item \begin{align*}1440\div12 - 888\times 0.125 &= \frac{12\times 120}{12}-111\times8\times0.125\\&=120-111\\&=9\end{align*}
\item \begin{align*}1.28+2.33+3.46+5.72+6.67+7.54&=(1.28+5.72)+(2.33+6.67)+(3.46+7.54)\\&=7+9+11\\&=27\end{align*}
\item \begin{align*}123456789\times987654321\times (25-125\times0.2) &= 123456789\times987654321\times(25-125\times\frac{1}{5})\\&=123456789\times987654321\times(25-25)\\&=123456789\times987654321\times0\\&=0\end{align*}
\item \begin{align*}2.34-(0.6+0.008\div 0.04)\times 0.3 &= 2.34 - (0.6+\frac{1}{125}\div\frac{1}{25})\times0.3\\&= 2.34-(0.6+\frac{1}{125}\times25)\times0.3\\&=2.34-(0.6+\frac{1}{5})\times0.3\\&=2.34-(0.6+0.2)\times0.3\\&=2.34-0.8\times0.3\\&=2.34-0.24\\&=2.1\end{align*}
\item \begin{align*}0.234\times 123+0.234+2012\times 0.234 +1.3\times 332\times 0.18&=0.234\times(123+1+2012)+(1.3\times0.18)\times332\\&=0.234\times2136+0.234\times332\\&=0.234\times(2136+332)\\&=0.234\times2468\\&=577.512\end{align*}
\item \begin{align*}2012\times 2-201.2\times 22.3 +20.12\times123&=2012\times2-2012\times0.1\times22.3+2012\times0.01\times123\\&=2012\times(2-0.1\times22.3+0.01\times123)\\&=2012\times(2-2.23+1.23)\\&=2012\times1\\&=2012\end{align*}
\item \begin{align*}(1.2\times 1.25 + 0.5)\div (0.25\times 8 - 0.008\times125)&=(0.3\times4\times1.25+0.5)\div(0.25\times4\times2 - 1)\\&=(0.3\times5+0.5)\div(2-1)\\&=2\end{align*}
\item \begin{align*}&125.25+101.75\times0.28+0.72\times101.5+0.72\times0.25\\&=125.25+(101.5+0.25)\times0.28+0.72\times101.5+0.72\times0.25\\&=125.25+101.5\times0.28+0.25\times0.28+0.72\times101.5+0.72\times0.25\\&=125.25+101.5\times(0.28+0.72)+0.25\times0.28+0.72\times0.25\\&=125.25+101.5+0.25\times(0.28+0.72)\\&=125.25+101.5+0.25\\&=125.5+101.5\\&=227\end{align*}
\item \begin{align*}&1.0+1.2+1.4+1.6+1.8+2.0+\cdots+9.6+9.8\\&=(1.0+1.2+1.4+1.6+1.8)+(2.0+\cdots+2.8)+\cdots+(9.0+\cdots+9.8)\\&=[1\times5+(0.2+0.4+0.6+0.8)]+[2\times5+(0.2+0.4+0.6+0.8)]+\cdots+[9\times5+(0.2+0.4+0.6+0.8)]\\&=(1\times5+2\times5+\cdots+9\times5)+9\times(0.2+0.4+0.6+0.8)\\&=5\times(1+2+3+\cdots+9)+9\times2\\&=5\times45+18\\&=225+18\\&=243\end{align*}
\item \begin{align*}1234\times314159\times4321\times(4\times0.25-0.008\times125) &= 1234\times314159\times4321\times(4\times\frac{1}{4}-\frac{1}{125}\times125)\\&=1234\times314159\times4321\times0\\&=0\end{align*}
\item \begin{align*}0.0625\times3.552\times4+0.8\times0.14 &= \frac{1}{16}\times4\times\frac{111}{125}\times4+\frac{4}{5}\times\frac{7}{50}\\&=\frac{111}{125}+\frac{14}{125}\\&=1\end{align*}
\item \begin{align*}0.\dot{2}\dot{4}\dot{6}\dot{8}+3.141592\dot{6}+0.125 &\approx 0.24682+3.14159+0.125 \\ &\approx 3.513 \mbox{ correct to 3 decimal places}\end{align*}
\item Solving this problem by \emph{only} brute force is infeasible. The strategy here is to count how many integers are there that have $x$ digits, and sum up the total number of digits. We start with $x=1$, and keep increasing the value of $x$ until the total number of digits is near to 2013.\\
When $x=1$, there are 9 such integers, total number of digits = $1\times9=9$, cumulative sum = 9.\\
When $x=2$, there are 90 such integers, total number of digits = $2\times90=180$, cumulative sum = 189.\\
When $x=3$, there are 900 such integers, total number of digits = $3\times900=2700$, cumulative sum = 2889.\\
Now, we can see 2889 is too far from 2013. So, we should take a few steps back. It is easy to deduce that from 100 to 199, inclusive, there are 300 digits. This is the same as ranges from 200 to 299, 300 to 399 and so on. Since we know that the 2889th digit is 9 of 999, we can reduce 2889 by 300 (which becomes 2589) so that the last digit now is 9 of 899. We can repeat this process until we find that the 1989th digit is 9 of 699. Next, all we need to do is to list down several integers after 699 and count the number of digits starting from 1990 until we reach 2013. This turns out to be 7 from 707.

\end{enumerate}
\end{document}
