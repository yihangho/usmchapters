\documentclass[10pt]{article} 

\usepackage{amsmath, amssymb, fullpage, graphicx}

\begin{document}
\title{Parity (Even and Odd)}
\maketitle
\setlength{\parindent}{0in}
\section{Important Facts}
Parity is the evenness or oddness of an integer. When we say the parity of $x$ and $y$ are the same, we means that either both $x$ and $y$ are even or both $x$ and $y$ are odd. To refresh, even numbers are $\{2, 4, 6, 8, \cdots\}$, while odd numbers are $\{1, 3, 5, 7, \cdots\}$. Generally, an integer is said to be even if and only if that integer can be written as $2k$, where $k$ is some integer. Similarly, an integer is odd if and only if that integer can be written as $2k+1$, where $k$ is some integer. Below are some properties of even and odd numbers:
\begin{enumerate}
\item
\begin{enumerate}
\item Odd $\pm$ Odd = Even
\item Odd $\pm$ Even = Odd
\item Even $\pm$ Even = Even
\end{enumerate}
\item
\begin{enumerate}
\item Odd $\times$ Odd = Odd
\item Odd $\times$ Even = Even
\item Even $\times$ Even = Even
\end{enumerate}
\item The parity of the sum and difference of two integers are the same.
\item The parity of two integers are different if and only if the sum and difference of these two integers is odd; the parity of two integers are the same if and only if the sum and difference of these two integers is even.
\item The sum of any two consecutive integers is always odd; the product of any two consecutive is always even.
\item The sum of oddly many odd integers is odd; the sum of evenly many odd integers is even.
\item The product of several integers is odd if and only if each of these integers is odd; the product of several integers is even if and only if there exists at least 1 even integers among these integers.
\end{enumerate}
These facts, although might seem trivial, is very useful when solving problems involving parity. Each of the above facts can be proven easily by using the definition of even and odd numbers.

\section{Examples}
\textbf{Example 1}\\
Can you choose 5 integers from $\{1, 3, 3, 5, 5, 5, 7, 7, 7, 7, \cdots\}$ such that these 5 integers add up to 100? If so, what are the integers?\\
\textit{Solution:}\\
Clearly, the given set of integers are all odd, and we are asked to pick 5 integers from this set such that they sum up to 100. But we also know that the sum of oddly many odd integers is always odd. Hence, it is impossible to pick oddly many (for this case, 5) odd integers such that they add up to an even integer (for this case, 100).\\
\textbf{Example 2}\\
Given that $2013a+10b=10095$, where $a$ and $b$ are positive integers. Find $a+b$.\\
\textit{Solution:}\\
First, notice that $10b$ is always even, but the sum of $2013a$ and $10b$ is odd. Hence, $a$ must be odd as well. If we were to try all possible values of $a$ and $b$, it will take a very long time to complete. So, we can find the maximum value for one of $a$ and $b$ and try all possible values less than this maximum value. Because $10<2013$, hence, the maximum value of $b$ will be much larger than that of $a$. We will find the maximum value of $a$. To find the maximum value of $a$, we need to minimize $b$, that is, let $b=1$, we get
\begin{align*}
2013a+10 &= 10095\\
2013a &= 10085\\
a &= \frac{10085}{2013}\\&=5.01
\end{align*}
Hence, we can say that $a\le5$ and $a$ is odd. This leaves us with only 3 possible values, that is, 1, 3 and 5 to try with. With some quick test, when $a=1$ or $a=3$, $b$ is not an integer. Hence, $a=5$, $b=3$, $a+b=8$.\\
\textit{Alternative Solution:}\\
Another solution exists and is based on some facts that is mentioned in \textit{Periodic Function}, that is $d(a+b)=d(d(a)+d(b))$, where $d(n)$ is the remainder of $n$ when divided my a constant $q$. Also notice that if $a=b$, then $d(a)=d(b)$. Now, let $q=5$. Since
\[2013a+10b=10095\]
\[\therefore d(2013a+10b)=d(10095)\]
\[d(d(2013a)+d(10b))=d(10095)\]
Since $10b$ and 10095 are always divisible by 5, hence $d(10b)=d(10095)=5$, or
\[d(2013a)=0\]
with further simplification,
\[d(3a)=0\]
With some trial, it is easy to notice that $a=5, 10, 15\cdots$, but when $a\ge10$, $2013a>10095$. Hence, $a=10$, with that, $b=3$, which agrees with the previous solution.\\
\textbf{Example 3}\\
Given a set of integers $\{4, 4, 4, 4, 36, 36, 36, 36, 100, 100, 100, 100, 196, 196, 196, 196\}$. Is it possible to pick 9 integers from this set such that they add up to 792?\\
\textit{Solution}\\
Notice that each of the numbers in the set can be written in the form of $4x$, where $x$ is an odd number. Hence, the sum of \emph{any} 9 integers from the set can be written as
\begin{align*}
4x_1+4x_2+4x_3+\cdots+4x_9 &= 4(x_1+x_2+x_3+\cdots+x_9)
\end{align*}
To satisfy the problem statement,
\[4(x_1+x_2+x_3+\cdots+x_9) = 792\]
or
\[x_1+x_2+x_3\cdots+x_9=198\]
But recall that $x_1, x_2, \cdots, x_9$ are odd and the sum of oddly many (9) odd integers is always odd. Hence, it is impossible to pick 9 integers from the set so that they sum up to 792.\\
\textbf{Example 4}\\
Given 25 cards, with one number from 1 to 25 written on each card such that no two cards have the same number written on it. Is it possible to arrange these cards around a circle so that the sum of numbers on any two neighboring cards is a prime number?\\
Hint: A prime number is a positive integer that have exactly 2 positive divisors.\\
\textit{Solution}\\
Notice that all prime numbers except 2 are odd (but not all odd numbers are prime). Since the sum of any two distinct positive integers is always greater than 2, hence, if such arrangement exists, the prime numbers obtained must be odd. In order for the sum of number on two neighboring cards to be odd, the parity of the number on these two cards must be different. To maintain such arrangement, the parity of the numbers on each card must alternate when the cards are arranged about a circle. But notice that from 1 to 25, there are more odd numbers. Hence, such arrangement does not exist.\\

\section{Exercises}
\begin{enumerate}
\item Find a set of three consecutive positive odd integers such that they add up to 309.
\item Find an example of three prime numbers $a$, $b$ and $c$ such that $a<b<c$ and $a+b=c$. After finding one such example, can you \emph{prove} if there exists infinitely many such triple? This is still an \emph{open} problem, which means it is still unsolved.\\
Hint: A prime number is a positive integer that have exactly 2 positive divisors.
\item Given that $a$, $b$, $c$ and $d$ are four distinct prime numbers such that $a+b+c=d$. What is the minimal value of $abcd$?
\item Given two integers $a$ and $b$, where one of $a$ and $b$ is 23 while the other is 32. It is known that $3a+2b$ is odd. What is $a$ and $b$?
\item Given that the difference between two two-digit number is 10. What can you say about the parity of:
\begin{enumerate}
\item the sum of these two two-digit number?
\item the sum of the four digits that appeared in the two two-digit number?
\end{enumerate}
\item Given a book with 100 pages. Jack tore out oddly many \emph{sheets} from this book and then sum up all the page numbers on the reamaining pages in the book. He said that the sum is 4096. Did Jack make a mistake when he sum up the page numbers? If not, what are the pages that were torn by Jack?\\
Hint: A \emph{sheet} consists of two \emph{pages}.
\item Given 280 stones. How many ways are there to evenly distribute these stones into:
\begin{enumerate}
\item evenly many baskets?
\item oddly many baskets?
\end{enumerate}
Note: Assume that you cannot differentiate one stone from another.
\item In the following equation, can you fill in + or - sign into each of the empty square so that the equation holds true?
\[1\Box2\Box3\Box4\cdots\Box20\Box21=22\]
\item Given a $8\times8$ chessboard. Can you lay some $1\times2$ dominos to cover the whole chessboard except any two white square? Notice that these dominos cannot overlap.
\item The product of a set of 2014 integers is 1. Can the sum of these numbers be 0?
\item Given $n$ gears arranged in a chain. Clearly, we know that if $n=3$, if is impossible to turn the gears. For what values of $n$ will the gears be able to turn?
\item Considering Example 4 above, if you were given $2n+1$ cards to write down numbers from 1 to $2n+1$, what will the result be?
\item Given numbers from 1 to 2013 written on a whiteboard. Jack the naughty do the following repeatedly: Choose any two numbers on the board, erase these numbers and write down their non-negative difference. He does so until there is only one number left on the board. Is it possible to predict the parity of the last number even before Jack starts erasing the numbers?
\item Referring to the previous question, given numbers from 1 to $n$ with Jack doing the same action, is it possible to predict the parity of the last number?
\end{enumerate}

\section{Answers to Exercises}
\begin{enumerate}
\item $\{101, 103, 105\}$
\item $a=2$, $b=3$ and $c=5$
\item 3135
\item $a=32$, $b=23$
\item
\begin{enumerate}
\item even
\item odd
\end{enumerate}
\item Yes, he made a mistake.
\item 
\begin{enumerate}
\item 12
\item 4
\end{enumerate}
\item No
\item No
\item No
\item $n$ must be even
\item Same as Example 4
\item Yes, it is an odd number.
\item Yes, when $n$ is even, the parity is $\frac{n}{2}$; when $n$ is odd, the parity is $\frac{n+1}{2}$.
\end{enumerate}

\section{Solutions to Exercises}
\begin{enumerate}
\item We know that consecutive odd numbers differ by 2. Therefore, let $x$, $x+2$ and $x+4$ be the odd numbers. 
\[x+(x+2)+(x+4)=309\]
\[3x+6=309\]
\[3x=303\]
\[x=101\]
Hence, the numbers are 101, 103 and 105.
\item As mentioned before, all prime numbers except 2 are odd. Hence, of $a$, $b$ and $c$, at most 1 is even. Certainly, we know that $c>a$ and $c>b$. Hence, $c\ne2$, which implies that $c$ is odd. If $c$ is odd, then one of $a$ and $b$ \emph{must} be even. This makes $a=2$. Rewriting the original equation with $a$ being substituted with 2,
\[b+2=c\]
One example is $(a, b, c) = (2, 3, 5)$. There is a conjecture called \textit{Twin Prime Conjecture} which states that there exists infinitely many such triple, but this conjecture has yet to be proven and hence remains \textit{open}.
\item It is easy to see that none of $a$, $b$, $c$ and $d$ is even. To minimize $abcd$, we need to minimize $d$ (as $d$ is the largest of the four primes, and smaller $d$ means smaller $a+b+c$, which reduces the maximum value of $abc$). The sum of the three smallest odd prime is $3+5+7 = 15$. Hence, $d>15$, which leaves us with the choices $17, 19, 23, \cdots$. It is easy to see that $a$, $b$ and $c$ can only be 3, 5, 7 or 11. We can verify that there is no set of $a$, $b$, $c$ that sums up to 17, but $3$, $5$, $11$ sums up to the next smallest prime, that is, 19. Their product is $3\times5\times11\times19=3135$.
\item It is trivial to check that when $a=32$, $b=23$, $3a+2b$ is even. Hence, $a=23$, $b=32$.
\item Let $a$ and $b$ be integers such that $10\le a,b\le99$ and $b-a=10$, or $b=a+10$. We have already mentioned that the parity of the sum and difference of two integers are the same. Hence, $a+b$ has the same parity as $b-a$, which is even. The other question that address the sum of digits require some knowledge learnt from \textit{Periodic Function}. We can easily see that $a$ and $b$ has the same last digit. Let this digit be $k$. Indeed, $k$ is the remainder of $a$ (and $b$) when divided by 10. As mentioned in \textit{Periodic Function}, we can write $a$ and $b$ as $10x+k$ and $10y+k$, respectively. By trying out a few values of $a$ and $b$, we can see that $x$ and $y$ are the penunltimate digit of $a$ and $b$, respectively. Hence, the sum of all four digits is actually $x+y+2k$. Since we know that $b=a+10$, we can rewrite this relation as
\[10y+k=10x+k+30\]
\[10y=10(x+3)\]
or
\[y=x+3\]
Substitute this into our expression for the sum of digits,
\[x+y+2k = x + x + 3 + 2k = 2(x+k+1)+1\]
which clearly is odd.
\item We can see a sheet as the sum of two integers, $n+(n+1)$, where $n$ is odd. Hence, the sum of pages is
\[[(1+2)+(3+4)+(5+6)+\cdots+(199+200)\]
Notice that there are evenly many pairs in the above expression, and the sum of each pair is odd. Tearing out a sheet means removing a pair from the expression. Since oddly many sheets are removed, hence, it leaves oddly many pages in the book. There remains oddly many pairs add sum up to an odd number. Hence, the remaining sum is odd as well.
\item When the stones can be distributed evenly into $k$ baskets, $k$ must be a factor of 280. Hence, this problem can be interpreted as finding the number of even factors and odd factors. It is trivial to list down all the factors of 280, which are 1, 2, 4, 5, 7, 8, 10, 14, 20, 28, 35, 40, 56, 70, 140 and 280. Hence, we can see that there are 12 even factors and 4 odd factors.
\item The answer is negative. Clearly, we can see that if we fill in + into each of the squares, we will get an odd number, say $2k+1$. Now, suppose we change the sign in front of an integer $m$ from + to $-$. The sum now becomes $2k+1-2m=2(k-m)+1$, which is still an odd number. Hence, we can conclude that the sum will always be an odd number no matter which boxes are filled with + or $-$. Since 22 is even, there is no configuration that can lead us to 22, or any even number in general.
\item We know that a $8\times 8$ chessboard has 32 white squares and 32 black squares. If we use some dominos to tile the chessboard, each domino will cover exactly 1 black square and 1 white square. If we are not allowed to cover any 2 white squares, we will have more black squares than white squares. Hence, when the dominos are not allowed to overlap, it is impossible to tile the whole chessboard except any two white squares.
\item In order for the 2014 integers to sum up to 0, we need to have exactly 1007 +1 and 1007 $-1$, but clearly, the product of these integers is $-1$. Hence, it is impossible to have such situation happen.
\item The gears can turn when and only when $n$ is an even numbers. Suppose we choose any gear in the chain and label it as $a_1$. Also, we label the gear to the right of $a_k$ as $a_{k+1}$, for all natural number $k$. We can see that all the even numbered gears turn in one direction while all the odd numbered gears turn in the other direction. Also, $a_1$ and $a_n$ must turn in opposite direction. Hence, $n$ must be even.
\item The result and explantion is same as Example 4. This is just a generalization of Example 4.
\item Yes. The last number is always odd. The reason is similar to Problem 8. The number at the end can be written as the same way in Problem 8, that is, filling in $+$ or $-$ into squares in between consecutive numbers. Hence, the parity of the last number is same as the parity of the sum of these 2013 numbers, which is odd.
\item Again, yes. All we need to do is to find the parity of $1+2+3+\cdots+n$. When $n$ is even, the parity is the same as the parity of $\frac{n}{2}$; when $n$ is odd, the parity is the same as the parity of $\frac{n+1}{2}$.
\end{enumerate}
\end{document}
