\documentclass[10pt]{article}

\usepackage{amsmath, amssymb, fullpage, graphicx, amsfonts}

\begin{document}
\title{Fill in the Blanks}
\maketitle
\setlength{\parindent}{0in}
\section{Important Facts}
In this chapter, we will take a close look at problems that involve filling in the empty blanks or $\Box$ in an arithmetic operation. Of course, we can try each and every possibility into the blanks and check if the arithmetic holds true. But the problem with this is that the number of possibilities grows \emph{very} quickly when more blanks are introduced. In this chapter, the scope of discussion will be addition, subtraction and multiplication.

Our strategy here will be to fill in those blanks that we are certain of the answer, then deduce the answer for other blanks. At times, we still will need to employ the technique of trial and error, but the way we do it is more educated, ie, we will not try those answers that are certainly wrong. Also, notice that there might be more than one answer to some problems.

When solving this type of problem, take note of the following, they might help you solve the problem faster:
\begin{enumerate}
\item Start analyzing the arithmetic from the unit digit. All subsequent digits are dependent of the result of the unit digit.
\item Identify the places where carrying over will occur.
\item Transform problems involving subtraction to addition.
\item Fill in all the blanks that you are certain of the answer.
\item By convention, a number cannot starts with the digit 0.
\item Get the divisibility tests handy.
\end{enumerate}

\section{Examples}
\textbf{Example 1}
\begin{center}
\begin{tabular}{ccccc}
   &      & $\Box$ & 5 & 1 \\
 + &      & $\Box$ & 7 & $\Box$ \\
\hline
   &$\Box$&      9 & 3 & $\Box$ \\
\end{tabular}
\end{center}
\textit{Solution}\\
Clearly, we cannot get much information from the unit digit. Let us move on the the second digit from the right. Now, we know that $5+7=12$, but the blank below 5 and 7 is 3. Hence, there must be carrying over at the unit digit. In other words, at the unit digit, we have
\[1+\Box>10\]
Clearly, we have only one answer to this inequality, that is, $\Box = 9$. Also, we can deduce the answer for the blank below 1 and 9, which is 0. Now, back to the second digit, clearly, there is carrying over. So, we need to add 1 to the fourth digit. We shall take a step forward and look at the fourth digit. There are no digit above the blank at the third row. Hence, we can say that there is carrying over at the third digit. In fact, we can deduce the value for the blank at the fourth digit immediately. Notice that the maximal value for each blank is at most 9. Hence, the maximal sum for the third digit is $9+9=18$. Observe that the for a two-number addition, the maximum carrying over is 1. In other words, the square at the fourth digit is 1. And as shown previously, the two blanks at the third digit are 9. The following is the answer to this problem:
\begin{center}
\begin{tabular}{ccccc}
	&   & 9 & 5 & 1 \\
  + &   & 9 & 7 & 9 \\
\hline
	& 1 & 9 & 3 & 0 \\
\end{tabular}
\end{center}
\textit{Note:} It is especially important to check you answer after solving it.\\

\textbf{Example 2}
\begin{center}
	\begin{tabular}{ccccc}
		   & $\Box$ & $\Box$ & $\Box$ & $\Box$ \\
		 + &        & $\Box$ & $\Box$ & $\Box$ \\
		\hline
		   &   2    &   0    &   1    &   3    \\
	\end{tabular}
\end{center}
\textit{Solution:}
This problem might at first seems very difficult, but it is not. Notice that all the digits that appear on the last row are relatively small. We might try to construct our answer so that carrying over is avoided for as much as possible.

The unit digits sum up to a number with 3 as its last digit. Also, it is possible to avoid carrying over. So, we can take $0+3$ or $1+2$ (and their reverse).

The tens digits sum up to a number with 1 as its last digit. Now, if there is carrying over in the unit digit, this leave us with only one possible choice here, that is $0+0$. Similarly, if there is no carrying over, we have also only one choice, that is, $0+1$.

The hundreds digits sum up a number with 0 as its last digit. Clearly, one possible answer to avoid carrying over is $0+0$. However, if we take this, the number in the second row will start with 0, which is not accepted by convention. Hence, this leaves us with no choice but to choose other pairs that result in carrying over, like $1+9$, $2+8$, etc.

There is only one box for us to fill in for the thousands digit. Hence, the empty box must be 1 (because there must be carrying over from the previous digit).

Hence, one of the possible answers is
\begin{center}
	\begin{tabular}{ccccc}
		   & 1 & 1 & 0 & 0 \\
		 + &   & 9 & 1 & 3 \\
		\hline
		   & 2 & 0 & 1 & 3
	\end{tabular}
\end{center}

\textbf{Example 3}
\begin{center}
	\begin{tabular}{cccc}
		     &    9   &    2   & $\Box$ \\
		 $-$ & $\Box$ & $\Box$ &    4   \\
		\hline
		     & $\Box$ &    7   &    8
	\end{tabular}
\end{center}
\textit{Solution}\\
Take note that this problem involves subtraction, not addition like the previous examples. However, our strategy here is to transform this problem into an addition problem so that we can solve it using way that we solved the addition problem.

We know that if
\[a-b=c\]
then
\[a=b+c\]

In this case, let $a = 92\Box$, $b = \Box\Box4$ and $c = \Box78$. Hence, we can rewrite our problem as
\begin{center}
	\begin{tabular}{cccc}
		   & $\Box$ & $\Box$ &    4   \\
		 + & $\Box$ &    7   &    8   \\
		\hline
		   &    9   &    2   & $\Box$
	\end{tabular}
\end{center}
and solve it using skills that we have developed to solve the addition problem.

First of all, the empty box in the third row for the unit digit is clearly 2, and there is a carry over here.

Next, at the tens digit, we know that any non-negative digit plus 7 is more than 2. Hence, we can say that
\[\Box + 7 + 1 = 12\]
The reason that 1 is added is that there is carrying over from the previous digit. So, we get $\Box = 4$. Again, there is a carrying over to the next digit.

The hundreds digit is a bit tricky. We know that there will not be carrying over in this digit. Hence,
\[\Box + \Box + 1= 9\]
Again, 1 is added because of the carrying over from the previous digit. We get
\[\Box + \Box = 8\]
One of the possible ways to solve this is to take $1+7$. Hence, the answer to this addition is
\begin{center}
	\begin{tabular}{cccc}
		   & 1 & 4 & 4 \\
		 + & 7 & 7 & 8 \\
		\hline
		   & 9 & 2 & 2
	\end{tabular}
\end{center}
Now, it is important that we rewrite this back to the subtraction form.
\begin{center}
	\begin{tabular}{cccc}
		     & 9 & 2 & 2 \\
		 $-$ & 1 & 4 & 4 \\
		\hline
		     & 7 & 7 & 8
	\end{tabular}
\end{center}

\textbf{Example 4}
\begin{center}
	\begin{tabular}{cccccc}
		          &   & $\Box$ & $\Box$ &    1   & $\Box$\\
		 $\times$ &   &        &        &        &    7  \\
		\hline
		          & 1 & $\Box$ &    0   & $\Box$ &    1
	\end{tabular}
\end{center}
\textit{Solution}
Although this problem involves multiplication, we can still use the same approach as in addition. We will first start with the unit. We want to find $\Box$ such that $\Box \times 7$ ends with 1, and $\Box$ is a single digit number. Hence, we can easily deduce that $\Box = 3$, with carrying over. Notice that in addition, carrying over is \emph{always} adding 1 to the next digit, but in multiplication, we can carry over larger values, like in this case, 2. Next, in the tens digit place, $\Box$ is the unit digit of $1\times7+2$. (The 2 comes from carrying over from the unit digit.) Hence, $\Box = 9$. There is no carrying over in this place. In the tens digit place, we have $\Box \times 7$ ends with 0. Clearly, $\Box$ can only be 0. The thousands digit place is very tricky. We have $\Box \times 7$ ends with $\Box$. Notice that these 2 $\Box$s are not necessarily the same. Looking at the next digit, we can conclude that there is a carrying over of exactly 1 in the thousands digit. In other words, we have $10 \le \Box \times 10 \le 19$. There is only 1 such value, that is, 2. Hence, filling in all the blanks, we have
\begin{center}
	\begin{tabular}{cccccc}
		          &   & 2 & 0 & 1 & 3 \\
		 $\times$ &   &   &   &   & 7  \\
		\hline
		          & 1 & 4 & 0 & 9 & 1
	\end{tabular}
\end{center}

\textbf{Example 5}
\begin{center}
	\begin{tabular}{ccccccc}
		         &   &   &    3   & $\Box$ & $\Box$ & $\Box$\\
		$\times$ &   &   &        &        &    8   & $\Box$\\
		\hline
		         &   & 2 & $\Box$ & $\Box$ & $\Box$ &    8  \\
		         & 2 & 4 & $\Box$ &    9   &    6   &       \\
		\hline
		         & 2 & 6 & $\Box$ & $\Box$ & $\Box$ &    8  \\
	\end{tabular}
\end{center}
\textit{Solution}\\
In general, solving this type of problem that involves multiplication with a two-digit number can be broken down to solving two multiplications each with a single digit number, follow by solving another addition problem. We can, of course, take use this approach to solve this problem, but there are too many empty spaces in the first two rows, which can generate too many possibilities.

Instead, we can exploit that fact that on the fourth row, there is only one empty space, and we know that the number formed in the fourth row is divisible by 8. (This is because $24\Box96 = 8 \times 3\Box\Box\Box$.) Now, recall that in the previous chapter, a number is divisible by 8 if and only if the number formed by the last three digits of that number is also divisible by 8. Hence, we know that $\Box96$ is divisible by 8. From there, we deduce that $\Box = 0$ or $8$.

We shall try each of the possibilities of $\Box$ in order to find $3\Box\Box\Box$. When $\Box = 0$, $3\Box\Box\Box = 24096 \div 8 = 3012$, which is plausible. When $\Box = 8$, $3\Box\Box\Box = 24896 \div 8 = 3112$, which, again, is plausible. Now, we are \emph{sure} that $3\Box\Box\Box = 3\Box12$. Filling in all the digits that we already know, we have
\begin{center}
	\begin{tabular}{ccccccc}
		         &   &   &    3   & $\Box$ &    1   &    2  \\
		$\times$ &   &   &        &        &    8   & $\Box$\\
		\hline
		         &   & 2 & $\Box$ & $\Box$ & $\Box$ &    8  \\
		         & 2 & 4 & $\Box$ &    9   &    6   &       \\
		\hline
		         & 2 & 6 & $\Box$ & $\Box$ & $\Box$ &    8  \\
	\end{tabular}
\end{center}
Next, we will analyse the empty box on the second row. We know that $3\Box12\times\Box$ ends with 8. Hence, we can conclude that the empty box on the second row is either 4 or 9. But when taking it to be 4 and multiply it with all possible values of $3\Box12$, we can never have the results start with 2, hence contradicting with the third row. We conclude that the empty box in the second row is 9. Now, we shall fill in all the values that we know again:
\begin{center}
	\begin{tabular}{ccccccc}
		         &   &   &    3   & $\Box$ &    1   &    2  \\
		$\times$ &   &   &        &        &    8   &    9  \\
		\hline
		         &   & 2 & $\Box$ & $\Box$ &    0   &    8  \\
		         & 2 & 4 & $\Box$ &    9   &    6   &       \\
		\hline
		         & 2 & 6 & $\Box$ & $\Box$ & $\Box$ &    8  \\
	\end{tabular}
\end{center}
Up to here, further analysis with lead us nowhere. But we know that the value of $3\Box12$ will manipulate everything. So, we can just try all possible values of $3\Box12$ to see which one is the correct answer. $3012\times89=268068$, $3112\times89=276968$. We can see that taking 3112 contradicts with the last row. Hence, the answer is
\begin{center}
	\begin{tabular}{ccccccc}
		         &   &   &    3   &    0   &    1   &    2  \\
		$\times$ &   &   &        &        &    8   &    9  \\
		\hline
		         &   & 2 &    7   &    1   &    0   &    8  \\
		         & 2 & 4 &    0   &    9   &    6   &       \\
		\hline
		         & 2 & 6 &    8   &    0   &    6   &    8  \\
	\end{tabular}
\end{center}

\section{Exercises}
\begin{enumerate}
\item Show that the following cannot be solved
	\begin{center}
		\begin{tabular}{cccc}
			   &   & $\Box$ & $\Box$ \\
			 + &   & $\Box$ & $\Box$ \\
			\hline
			   & 1 &    9   &    9
		\end{tabular}
	\end{center}
\item Solve the following
\begin{center}
	\begin{tabular}{cccc}
		   &   & $\Box$ & $\Box$ \\
		 + &   & $\Box$ & $\Box$ \\
		\hline
		   & 1 &    9   &    5   \\
	\end{tabular}
\end{center}
\item Solve the following
\begin{center}
	\begin{tabular}{cccc}
		   &   &        & $\Box$ \\
		   &   & $\Box$ & $\Box$ \\
		 + &   & $\Box$ & $\Box$ \\
		\hline
		   & 2 &    0   &    5
	\end{tabular}
\end{center}
\item Let $x=\overline{abcd}$ be a four-digit number. Find the largest possible $x$ such that the following can be solved
\begin{center}
	\begin{tabular}{ccccc}
		   &     & $\Box$ & $\Box$ & $\Box$ \\
		 + &     & $\Box$ & $\Box$ & $\Box$ \\
		\hline
		   & $a$ &   $b$  &   $c$  &   $d$  \\
	\end{tabular}
\end{center}
\item Given that in the following subtraction, each of the digits from 0 to 9 appears exactly once. Solve the following
\begin{center}
	\begin{tabular}{ccccc}
		     & 1 &    0   &    3   &    5   \\
		 $-$ &   & $\Box$ & $\Box$ & $\Box$ \\
		\hline
		     &   & $\Box$ & $\Box$ & $\Box$
	\end{tabular}
\end{center}
\item Solve the following
\begin{center}
	\begin{tabular}{ccccc}
		     & $\Box$ & $\Box$ &    8   &    5   \\
		 $-$ &        & $\Box$ & $\Box$ & $\Box$ \\
		\hline
		     &        &        & $\Box$ &    1   \\
	\end{tabular}
\end{center}
\item It is given that $a=\overline{w_1x_1y_1z_1}$ and $b=\overline{w_2x_2y_2z_2}$ satisfy the following
\begin{center}
	\begin{tabular}{ccccc}
		     & $w_1$ & $x_1$ & $y_1$ & $z_1$ \\
		 $-$ & $w_2$ & $x_2$ & $y_2$ & $z_2$ \\
		\hline
		     &   8   &   9   &   1   &   3
	\end{tabular}
\end{center}
Find the maximal value of $a+b$.
\item Let $x=\overline{abcd}$. Given that $x$ satisfies the following multiplication, what is the largest possible value of $x$?
\begin{center}
	\begin{tabular}{cccccc}
		          &        &   $a$  &   $b$  &   $c$  &   $d$ \\
		 $\times$ &        &        &        &    1   &    1  \\
		\hline
		          & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$
	\end{tabular}
\end{center}
\item Given that each of the following empty boxes represents the same digit. Solve the following
\begin{center}
	\begin{tabular}{cccccc}
		          &   & $\Box$ & $\Box$ & $\Box$ & $\Box$ \\
		 $\times$ &   &        &        &        & $\Box$ \\
		\hline
		          & 8 & $\Box$ & $\Box$ & $\Box$ &    1
	\end{tabular}
\end{center}
\item Solve the following
\begin{center}
	\begin{tabular}{cccccc}
		          &        &    3   & 0 & 7 & $\Box$ \\
		 $\times$ &        &        &   &   & $\Box$ \\
		\hline
		          & $\Box$ & $\Box$ & 3 & 0 &    0
	\end{tabular}
\end{center}
\item Solve the following
\begin{center}
	\begin{tabular}{ccccccc}
		          &        &        & $\Box$ & $\Box$ & $\Box$ & $\Box$ \\
		 $\times$ &        &        &        &        &    9   & $\Box$ \\
		\hline
		          &        & $\Box$ & $\Box$ & $\Box$ & $\Box$ &    0   \\
		          &    7   & $\Box$ &    7   &    7   &    4   &        \\
		\hline
		          & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$
	\end{tabular}
\end{center}
\item Solve the following, using the fact that all the occurrences of 7 are shown. Solve the following
\begin{center}
	\begin{tabular}{cccccc}
		          &        &        &    7   & $\Box$ &    7   \\
		 $\times$ &        &        &        & $\Box$ &    7   \\
		\hline
		          &        & $\Box$ & $\Box$ &    7   & $\Box$ \\
		          & $\Box$ & $\Box$ &    7   & $\Box$ &        \\
		\hline
		          & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ \\
	\end{tabular}
\end{center}
\item Solve the following
\begin{center}
	\begin{tabular}{cccccc}
		          &        &        & $\Box$ & $\Box$ & $\Box$ \\
		 $\times$ &        &        &        &    9   &    2   \\
		\hline
		          &        &        & $\Box$ &    5   &    2   \\
		          &    1   & $\Box$ & $\Box$ & $\Box$ &        \\
		\hline
		          & $\Box$ & $\Box$ & $\Box$ & $\Box$ & $\Box$ \\
	\end{tabular}
\end{center}
\item In the multiplication below, it is known we can fill in \emph{all} five \emph{distinct} odd digits into the empty boxes. Show that there is only one such configuration.
\begin{center}
	\begin{tabular}{ccc}
		          & $\Box$ & $\Box$ \\
		 $\times$ &        & $\Box$ \\
		\hline
		          & $\Box$ & $\Box$
	\end{tabular}
\end{center}
\end{enumerate}

\section{Answers to Exercises}
Note: For problems that have an asterisk (*) next to the problem number, these are the problems that have multiple possible answers.
\begin{enumerate}
\item The maximal sum of two two-digit number can only be 198.
\item *
\[97+98=195\]
\item *
\[7+99+99=205\]
\item 1998
\item *
\[1035-246=789\]
\item
\[1085-994=91\]
\item 11085
\item 9090
\item
\[9999\times11=89991\]
\item
\[3075\times4=12300\]
\item
\[8086\times95=40430+727740=768170\]
\item
\[797\times87=5579+63760=69339\]
\item *
\[126\times92=252+11340=11592\]
\item
\[19\times3=57\]
\end{enumerate}
\end{document}