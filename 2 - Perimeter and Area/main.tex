\documentclass[10pt]{article} 

\usepackage{amsmath, amssymb, fullpage, graphicx}

\begin{document}
\title{Perimeter and Area}
\maketitle
\setlength{\parindent}{0in}
\section{Important Facts}
Up to this point, you might have learnt various geometrical shapes like square, rectangle, rhombus, triangles, etc. Problems that involve these geometrical shapes include calculating perimeter and area. While there are many formulae exist for each of the shapes mentioned earlier on, it is often useful to be able to calculate the perimeter and area of these shapes without using such complicated formulae. 
\subsection*{Perimeter}
Perimeter of a geometrical shape is the length of path that surrounds the geometrical shape. In other words, it is the sum of length of all sides of that geometrical shape. Sometimes, it is impossible to work out the length of a particular side, but when analyzed together with some other sides, their combined length is trivially found. 
\subsection*{Area}
When calculating area of a geometrical shape with most its internal angles equal to some multiple of $90^\circ$, it is often sufficient to know how to calculate the area of a rectangle, the area of a right angle triangle, and the following facts:
\begin{enumerate}
\item Two objects that can completely overlap each other have the same area.
\item When an object is cut into two, the sum of areas of the two smaller area equals to the area of the original object.
\end{enumerate}
The significance of the second point is that we can \emph{cut} a geometrical shape into arbitararily many smaller shapes and compute the area of each of the smaller shapes. This is the core idea in solving problems involving area of a geometrical shape that does not have a formula.

\section{Examples}
\subsection*{Perimeter}
\textbf{Example 1}\\
Calculate the perimeter of the following figure:
\begin{center}\includegraphics{./diagrams/1.png}\end{center}
Comment: Following the method of trying to find the length of each and every side of the figure, the lengths of $BC$, $CD$, $DE$, and $EF$ cannot be determined. But as mentioned earlier on, we can analyze several sides together.\\
\textit{Solution}\\
\begin{center}\includegraphics{./diagrams/2.png}\end{center}
Let $C'$ and $E'$ be points on $AF$ and $AB$, respectively, such the $C'$ is the intersection point of lines $AF$ and $CD$ when extended and $E'$ is the intersection point of lines $AB$ and $DE$ when extended. Now, notice that $BC = AC'$ and $DE = C'F$. Therefore,
\[BC+DE = AC'+C'F = AF = 20\mbox{ units}\]
Similarly, $FE=AE'$ and $DC=E'B$, which implies
\[FE+DC = AE'+E'B = AB = 15\mbox{ units}\]
Hence, it is clear that the perimeter of this figure is $20+20+15+15=70\mbox{ units}$.\\
This problem can be further generalized, as illustrated in the next example.\\
\textbf{Example 2}\\
We define a $2k+2$-gon as a $k$-step when $k+3$ of the internal angles are exactly $90^\circ$ and the remaining $k-1$ internal angles are exactly $270^\circ$, also, a $k$-step resembles the cross-section of a staircase that takes $k$ steps to travel from the bottom to the top. As such, the diagram in Example 1 is a $2$-step. Diagram below shows a $5$-step.
\begin{center}\includegraphics{./diagrams/3.png}\end{center}
Find the perimeter of a $2012$-step that is 2011 units high and 2013 units wide.\\
Comment: This Example is a generalization of Example 1. The tactic that is employed here is exactly the same as that of Example 1.\\
\textit{Solution}\\
\begin{center}\includegraphics{./diagrams/4.png}\end{center}
Suppose the diagram above shows a $2012$-step. We can mark 2011 points on each of $AB$ and $AL$ just like we did in Example 1 such that line segment formed by any two consecutive points on each of the line represents either the height or the width of a step. Hence, the combined height and width of all the steps equals to the heights and width, respectively, of $2012$-step. Hence,
\[\mbox{Perimeter} = 2\times(2011+2013)=8048\mbox{ units}\]
\textbf{Example 3}\\
Diagram below shows a big rectangle that is made up of 7 smaller and congruent rectangles. Given that the width of the bigger rectangle is 10 units.
\begin{center}\includegraphics[width=200px]{./diagrams/5.png}\end{center}
Find the perimeter of the big rectangle.\\
\textit{Solution}\\
Let $w$ and $h$ be the width and height of each small rectangle, respectively. Clearly, $2w = 10$. Notice that the width of the big rectangle $=2w=5h$.
\[\therefore h = \frac{1}{5}2w = \frac{1}{5}\times10 = 2\mbox{ units}\]
Now,
\[\mbox{Perimeter} = 2\times(2w+w+h)=2\times(3w+h)=2\times(15+2)=34\mbox{ units}\]
\subsection*{Area}
\textbf{Example 4}\\
Find the area of the polygon below.
\begin{center}\includegraphics{./diagrams/6.png}\end{center}
\textit{Solution}\\
The strategy that we employ here is to cut the polygon into several pieces of rectangle, as shown below:
\begin{center}\includegraphics{./diagrams/7.png}\end{center}
\[\mbox{Area of ABCJ} = 15\times1=15\mbox{ units}^2\]
\[\mbox{Area of JDEI} = 8\times2=16\mbox{ units}^2\]
\[\mbox{Area of IFGH} = 3\times7=21\mbox{ units}^2\]
\[\therefore\mbox{Area of ABCDEFGH} = 15+16+21=52\mbox{ units}^2\]
Comment: There is no \emph{one} correct cutting. For example, each of the following cuttings will yield the same correct answer.
\begin{center}\includegraphics[width=500px]{./diagrams/8.png}\end{center}
\textbf{Example 5}\\
Given that when the height of a rectangle is reduced by 4 units, or when the width of the same rectangle is reduced by 5 units, the area decreases by 60 units$^2$. Find the area of the rectangle.\\
\textit{Solution}\\
\begin{center}\includegraphics{./diagrams/9.png}\end{center}
From the problem statement, the area of shaded region in the diagram above is 60 units$^2$.
\begin{align*}
AE\times AB&=60\\
4\times AB &=60\\
AB &= 15\mbox{ units}
\end{align*}
\begin{center}\includegraphics{./diagrams/10.png}\end{center}
Similarly, the area of shaded region in this diagram is also 60 units$^2$.
\begin{align*}
BE\times BC &= 60\\
5\times BC &= 60\\
BC &= 12\mbox{ units}
\end{align*}
Now, we know that the area of this rectangle is $AB\times BC$
\[AB\times BC = 12\times 15 = 180\mbox{ units}^2\]

\section{Exercises}
\begin{enumerate}
\item In the diagram below, the height and width of the cross are both 10 units. Calculate the perimeter of this cross.
\begin{center}\includegraphics{./diagrams/11.png}\end{center}
\item Find the perimeter of a 123456789-step of 10 units high, 15 units wide. (For the definition of a k-step, refer to Example 2 above.)
\item In the diagram below, the polygon shown is made up of three congruent rectangle. The length of the longer side of each rectangle is 10 units while the length of the shorter side is 7 units. What is the perimeter of the whole polygon?
\begin{center}\includegraphics{./diagrams/12.png}\end{center}
\item Given a piece of wire of 10 units long. In how many ways can we bend the wire into the shape of a rectangle of integer side lengths? (Note: consider rectangle of $a\times b$ not the same as rectangle of $b\times a$ if $a\ne b$.)
\item Referring the the previous question, in how many ways can we bend the wire if the length of the given wire was $2k$, where $k$ is any natural number?
\item In the diagram below, the square is cut into 3 congruent rectangles, each of perimeter 32 units. What is the perimeter of the square?
\begin{center}\includegraphics{./diagrams/13.png}\end{center}
\item Referring to the previous question, what is the perimeter of the square if the perimeter of each rectangle was $8k$ units, where $k$ is any natural number?
\item Consider a 5-step as shown in the diagram below, we define $BC$, $DE$, $FG$, $HI$ and $JK$ as the height of each step, and $CD$, $EF$, $GH$, $IJ$ and $KL$ as the width of each step. (For the definition of a k-step, refer to Example 2 above.)
\begin{center}\includegraphics{./diagrams/3.png}\end{center}
Find the area of a 6-step that has equal width and height for each step. Given that the width and height of each step are 2 units and 5 units, respectively. Find the area of this 6-step.
\item In the diagram below, the side lengths of the squares are 5 units and 10 units, respectively. What is the area of shaded region?
\begin{center}\includegraphics[width=200px]{./diagrams/14.png}\end{center}
\item 4 congruent rectangles are arranged to form a big square, with an empty square inside, as shown in the diagram below. Given that the area of the big square is 100 units$^2$ and the area of the small square is 1 unit$^2$. What is the dimension of each rectangle?
\begin{center}\includegraphics[width=200px]{./diagrams/15.png}\end{center}
\item Referring to the previous question, what is the dimension of each rectangle if the area of the big square was $m^2\mbox{ units}^2$ and the area of the small square was $n^2\mbox{ units}^2$, where both $m$ and $n$ are integers and $m>n$?
\item Given a polygon has 4 sides with each of its internal angle equal $90^\circ$ has a perimeter of 36 units. Assuming that the length of each side of this polygon is an integer. What is the maximum possible area for this polygon?
\item In the diagram below, the width and height of rectangle $ABCD$ are 10 units and 15 units, respectively. What is the area of shaded region?
\begin{center}\includegraphics{./diagrams/16.png}\end{center}
\item In the diagram below, given that the $ABCD$ is a square with side length 12 units and $BE = 5\mbox{ units}$. $CFGE$ is a rectangle, its width $CE = 13 \mbox{ units}$. Find the length of $CF$.
\begin{center}\includegraphics{./diagrams/17.png}\end{center}
\end{enumerate}

\section{Write Your First Mathematical Proof}
If you are reading this chapter (which you certainly are), chances are you have never written a mathematical proof. A mathematical proof is used to argue that a statement is true. A mathematical proof has to be rigorous, that means, the proof is built upon very strong foundation of known true facts. You can think of writing a mathematical proof as building a house. To build a house, each and every single piece of brick used must be strong; to write a mathematical proof, each and every single statement must already be proved to be true (can you see the recurrence relation here?). When a statement is proved to be correct, its truth is unquestionable and remains \emph{forever}.

Let's get started. Here, you will be guided to prove the Pythagorean Theorem. The Pythagorean Theorem states that in a right-triangle with side lengths $a$, $b$ and $c$ such that $a \le b < c$, the following holds true
\[a^2+b^2=c^2\]
As we have mentioned before, writing a proof is like building a house. So, we have broken down the proof to some simpler facts that you have learnt in this chapter. Answer each of the following question and combine them to prove the Pythagorean Theorem.
\begin{enumerate}
\item Let $ABCD$ be a square with side length $a+b$. What is the area of $ABCD$?
\item Let $E$, $F$, $G$ and $H$ be points on $AB$, $BC$, $CD$ and $DA$, respectively, such that $AE=BF=CG=DH=a$ and $BE=CF=DG=AH=b$, as shown in the diagram below.
\begin{center}\includegraphics{./diagrams/18.png}\end{center}
Are the areas of $\Delta AEH$, $\Delta DHG$, $\Delta CGF$ and $\Delta BFE$ the same? What is the area of each of the triangles mentioned?
\item Let $EF=FG=GH=HE=c$. What is the area of $EFGH$, given that $EFGH$ is a square?
\item What is the area of $ABCD$ written as the sum of the areas of the four triangles and $EFGH$?
\item Compare the answer that you found in (1) and (4). Try to simplify them and you should be able to get the Pythagoras' Theorem, that is, $a^2+b^2=c^2$.
\end{enumerate}

\section{Answers to Exercises}
\begin{enumerate}
\item 40 units
\item 50 units
\item 74 units
\item 4
\item $k-1$
\item 48 units
\item 12k units
\item 210 units$^2$
\item 37.5 units$^2$
\item 4.5 units $\times$ 5.5 units
\item $\frac{m-n}{2}\mbox{ units}\times\frac{m+n}{2}\mbox{ units}$
\item 81 units$^2$
\item 75 units$^2$
\item $\frac{144}{13}$ units
\end{enumerate}

\section{Solutions to Exercises}
\begin{enumerate}
\item We can see, in the diagram below, that $KL+AB+CD=JI+HG+FE=10$ and $AL+KJ+IH=BC+DE+HG=10$. Hence, the perimeter is 40 units.
\begin{center}\includegraphics{./diagrams/11.png}\end{center}
\item As we have established in Example 2, the perimeter of a $k$-step is independent of the value of $k$, but the height and width of this $k$-step. Hence, the perimater of such figure is 50 units.
\item In this problem, we can actually calculate the length of each line segment and then find the perimeter of the figure by adding up the length of each segment. However, we will use our usual method that is more elegant. Clearly, $HJ+FG=AB+CD=17$. Similarly, $BC+DF=AJ+HG=20$. Therefore, the perimeter of the figure is 74 units.
\begin{center}\includegraphics{./diagrams/12.png}\end{center}
\item Let $a$ and $b$ be the width and height, respectively, of the rectangle. We know that $2(a+b)=10$. Hence
\[a+b=5\]
Since we are limited to have $a$ and $b$ as positive integers, we can only have $(a, b)=(1, 4), (2, 3), (3, 2), (4, 1)$. So, we can form 4 difference rectangles given 10 units of wire.
\item Now, we have 
\[2(a+b)=2k\]
or
\[a+b=k\]
Clearly, $a$ can take values $1, 2, 3, \cdots, k-1$ and $b$ can take values $k-1, k-2, k-3, \cdots, 1$. Hence, we have $k-1$ different rectangles.
\item Let $h$ be the height and $w$ be the width of the small rectangle. In this case, we define height as the shorter side. Notice that the side length of the big square is $w$, but it is also the same as $3h$. Hence, we have
\[w=3h\]
and also give
\[2(w+h)=32\]
or
\[w+h=16\]
Substitute $w=3h$,
\[4h=16\]
\[h=4\]
Hence, perimeter $=4w=12h=48\mbox{ units}$.
\item Using the similar representation for height and width as the previous problem, we now have
\[2(w+h)=8k\]
\[w+h=4k\]
With $w=3h$ still holds true,
\[4h=4k\]
\[h=k\]
Perimeter $=4w=12h=12k$.
\item Notice that the area, unlike perimeter, of a $k$-step is dependent on the value of $k$ and also the height and width of each step. We have filled in the measurements that can be deduced directly from the problem statement in the diagram below.
\begin{center}\includegraphics{./diagrams/19.png}\end{center}
With the cuttings drawn and measurements labelled, it is now easy to find the area of each segment, which are 60 units$^2$, 50 units$^2$, 40 units$^2$, 30 units$^2$, 20 units$^2$ and 10 units$^2$, respectively. Hence, the total area or the area of the whole figure is 210 units$^2$.
\item It is possible to calculate the area of the shaded region directly, or to use area of the unshaded region to find the area of the shaded region. We will use the latter.
\begin{center}\includegraphics{./diagrams/14.png}\end{center}
We know that the total area of the whole figure is $5\times5+10\times10=125\mbox{ units}^2$. Let $k$ be the area of the shaded region. The area of the unshaded region is
\begin{align*}
\Delta AED + \Delta EFG &= \frac{1}{2}\times5\times(5+10) + \frac{1}{2}\times10\times10\\&=37.5+50\\&=87.5\mbox{ units}^2
\end{align*}
Now, we know that
\[87.5+k=125\]
Hence,
\[k=125-87.5=37.5\mbox{ units}^2\]
\item With the areas of the large square and the small square, we know that the side length of the big square is 10 units while the side length of the small square is 1 units. Let $a$ and $b$ be the side lengths of each rectangle, with $a<b$. We can say that $a+b=10$, or $b=10-a$. Also, we have the following:
\[4ab+1=100\]
From $b=10-a$, we have
\[4a(10-a)+1\]=100
which simplifies to
\[4a^2-40a+99=0\]
Such equation is known as quadratic equation is the way beyond the scope of this chapter. We will detour a little and take another approach. 
\begin{center}\includegraphics[width=200px]{./diagrams/20.png}\end{center}
Notice that $GK+KL=DH=b$, but also know that $KL=1$, $GK=a$, hence
\[a+1=b\]
together with $b=10-a$,
\[2a=9\]
\[a=4.5\]
we can now get $b=5.5$.\\
Comment: The spirit behind this problem is to think outside of the box. Although the method involving quadratic equation is the ``standard" method to most of the more experienced readers, and solving a quadratic equation is easy to them, the method that we present here is even simpler.
\item Using the method (and also the diagram) in the previous problem, we have $a+b=m$ and $b=a+n$. Combining these two equations we have
\[2a+n=m\]
or
\[a=\frac{m-n}{2}\]
and
\[b=\frac{m+n}{2}\]
\item Such polygon described in the problem is actually a rectangle. Notice that a square is also considered a rectangle. The reason behind phrasing such definition is to prevent readers who do not know (or agree) that a square is also a rectangle from misunderstanding the problem statement. Let $a$ and $b$ be the side lengths of this rectangle. We have
\[2(a+b)=36\]
or
\[a+b=18\]
We want to find the maximum value of $ab$. Since there are only 17 possible values of $a$ and $b$, we can just plug in all possible values and take the maximal one, which is 81. However, when given in general $a+b=k$, for some large (and not necessarily integer) $k$ and $a$ and $b$ not limited to integers, how can we find the maximal of $ab$? The answer is, for any given $k$ such that $a+b=k$, the maximal value for $ab$ is always $\frac{k^2}{4}$. We shall prove this statement. Suppose the contrary, that is, there are some other values of $a$, $b$ that gives us a larger $ab$, that is 
\[ab>\frac{k^2}{4}\]
Since we know that $a+b=k$, we can rewrite the above inequality as
\[a(k-a)>\frac{k^2}{4}\]
\[4a(k-a)>k^2\]
\[4ak-4a^2>k^2\]
\[k^2-4ak+4a^2<0\]
The left side of the inequality can be written as $(k-a)(k-a)$, or $(k-a)^2$, that is,
\[(k-a)^2<0\]
This is absurd as any number that can be written in the form of $n^2$ must be at least 0, but the expression above implies the opposite. A contradiction is achieved, hence, our initial assumption, that is, there are some other value of $a$, $b$ that gives us $ab>\frac{k^2}{4}$ while satisfying $a+b=k$.
\item Let $h$ be the height of the rectangle.
\begin{align*}\mbox{Area of shaded region } &= \frac{1}{2}\times AY_1\times h + \frac{1}{2}\times Y_1Y_2\times h + \frac{1}{2}\times Y_2Y_3 \times h + \frac{1}{2}\times Y_3Y_4 \times h + \frac{1}{2}\times Y_4B\times h\\&=\frac{1}{2}\times h\times (AY_1+Y_1Y_2+Y_2Y_3+Y_3Y_4+Y_4B)\\ &= \frac{1}{2}\times h\times AB \\ &= \frac{1}{2}\times 15\times 10 \\ &=75\mbox{ units}^2\end{align*}
\item Let $G$ be a point on $CE$ such that $DG$ and $CE$ are perpendicular to each other. Referring to the diagram below, the shaded triangle will help us determine the length of $CF$.
\begin{center}\includegraphics{./diagrams/21.png}\end{center}
In $\Delta CDE$, taking $CD$ as the base, we know the height is 12 units. Hence, $\Delta CDE = \frac{1}{2}\times12\times12=72\mbox{ units}^2$. Now, using the same triangle, taking $CE$ as its base, the height is $DG$, 
\[\Delta CDE = \frac{1}{2} \times 13 \times DG\]
But we also know that $Delta CDE = 72\mbox{ units}^2$, thus,
\[\frac{1}{2}\times13\times DG=72\]
or
\[DG = \frac{144}{13}\mbox{ units}\]
Notice that $CF=DG$. Hence, $CF=\frac{144}{13}\mbox{ units}$.
\end{enumerate}

\section{Answers to Writing Your First Mathematical Proof}
\begin{enumerate}
\item $a^2+2ab+b^2$
\item They are the same. The area is $\frac{ab}{2}$.
\item $c^2$
\item $2ab+c^2$
\item $a^2+b^2=c^2$
\end{enumerate}
\end{document}
