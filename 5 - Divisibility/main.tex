\documentclass[10pt]{article} 

\usepackage{amsmath, amssymb, fullpage, graphicx, amsfonts}

\begin{document}
\title{Divisibility}
\maketitle
\setlength{\parindent}{0in}
\section{Important Facts}
We do division \emph{everyday}. For example, you have 4 hours to study 3 subject. How long do you have to study each subject? You have a book of 100 pages, and you notice that you cannot finish the whole book in one month if you read exactly the same amount of pages everyday. These situations involve divisibility. And here, we will discuss specifically the divisibility of \emph{integers}. 
\subsection*{Definition of divisibility}
When we say that $a$ is divisible by $q$, where $a$ and $q$ are integers, there must exist another integer $b$ such that 
\[a=bq\]
or in other words,
\[\frac{a}{q} \mbox{ is an integer}\]
We can also write the statement ``$a$ is divisible by $q$" by ``$q|a$". Notice the order of the two numbers when written in the latter form. ``$q|a$" is read as ``$q$ divides $a$ completely", which effectively means $a$ is divisible by $q$. On the other hand, if $a$ is not divisible by $q$, we write $q\nmid a$, which is read as ``$q$ does not divide $a$ completely".

\subsection*{Implications of divisibility}
Suppose that $q|a$ and $b=\frac{a}{q}$. The following hold true:
\begin{enumerate}
\item $b$ is an integer
\item $q$ and $b$ are factors of $a$
\item $a$ is divisible by $b$ as well
\item $a$ is a multiple of $q$
\item $q|(an)$ for any integer $n$
\end{enumerate}
Now, if $q|a$ and $q|c$, then $q|(a\pm c)$. In this case, we say that $q$ is a common divisor of $a$ and $c$. A common divisor of two or more integers is an integer that divides each of these two or more integers. We define $\gcd(a, b)$ to be the \emph{greatest} common divisor of $a$ and $b$, that is, the largest integer that divides both $a$ and $b$. For example, $\gcd(24, 2013)=3$. When $\gcd(a, b)=1$, we say that $a$ and $b$ are relatively prime, or that they are \emph{coprime}. Suppose that $a$ and $b$ are relatively prime and $q|ab$, then exactly one of $a$ and $b$ is divisible by $q$. That is , if $\gcd(a, b) = 1$ and $q|ab$, then $q|a$ or $q|b$, but not both $q|a$ and $q|b$ are true.

Let $x$ and $y$ be two relatively prime integers such that $x|a$ and $y|a$. We can say that $xy|a$.

\subsection*{Divisibility Test}
Given that $a$ and $q$ are integers. Often, we are only interested in determining whether $a$ is divisible by $q$, but not the value of $\frac{a}{q}$. So, is there any way to determine the former, without evaluating the latter? The answer is positive. There are certain tests that can be carried out on $a$ for some specific values of $q$ to determine if $a$ is divisible by $q$. These tests are known as the divisibility tests. Divisibility tests are useful in the sense that, relatively, they are much easier than evaluating $\frac{a}{q}$. Before learning those tests, we shall define some terminologies that will be used extensively. 
\begin{enumerate}
\item The last digit of a number is the unit digit of that number. For example, the last digit of 2013 is 3.
\item The number formed by the last $k$ digits of a number is a number formed by deleting all but the last $k$ digits from the given number. For example, the number formed by the last 5 digits of 1234567890 is 67890.
\item The sum of digits of a number is the sum of all digits that form the given number. For example, the sum of digits of 123321 is $1+2+3+3+2+1=12$.
\item The alternate sum of digits of a number is value formed when all the digits from the given number is written in a row with the signs $+$ and $-$ inserted between any two consecutive digits alternately. For example, the alternate sum of digits of 12345 is $+1-2+3-4+5=3$.
\end{enumerate}
The following are the divisibility tests for $q\le 12$. Each of the following describes the property of $a$ such that $q|a$ for values of $q$ given at the beginning of each statement.
\begin{itemize}
\item When $q=2$, the last digit of $a$ is even.
\item When $q=3$, the sum of digits of $a$ is divisible by 3.
\item When $q=4$, the number formed by the last two digits of $a$ is divisible by 4.
\item When $q=5$, the last digit of $a$ is 0 or 5.
\item When $q=6$, $a$ is divisible by 2 and 3.
\item When $q=7$, double the last digit of $a$, and subtract this value from the number formed by deleting the last digit of $a$. The evaluated value is divisible by 7. For example, let $a=91$. Double the last digits ($1\times2=2$) and subtract this value from the number formed by deleting the last digit (this number is 9, found by deleting 1 from 91). The evaluated value is $9-2=7$, which is divisible by 7. Hence, 91 is divisible by 7.
\item When $q=8$, the number formed by the last 3 digits of $a$ is divisible by 8.
\item When $q=9$, the sum of digits of $a$ is divisible by 9.
\item When $q=10$, the last digit of $a$ is 0.
\item When $q=11$, the alternate sum of digits of $a$ is divisible by 11.
\item When $q=12$, $a$ is divisible by 3 and 4.
\end{itemize}
Now, notice that for certain values of $q$, the test has to be carried out a few times before we can form a conclusion. For example, when $q=11$, when $a$ is sufficiently large, its alternate digit sum can also be quite large that we cannot determine by sight if the alternate digits sum is divisible by 11. Hence, we run the test again on the alternate digit sum recursively until we can easily tell if the test subject is divisible by 11. Also, there might exists several different tests for one value of $q$. The use of which test is not important. What is more important is, it should be proved that the test used is mathematically true. 

Some careful observations might suggest that some of the tests above can be generalized. The following are true, for all non-negative integer $n$:
\begin{itemize}
\item When $q=2^n$, the number formed by the last $n$ digits of $a$ is divisible by $2^n$.
\item When $q=5^n$, the number formed by the last $n$ digits of $a$ is divisible by $5^n$.
\item When $q=10^n$, the last $n$ digits of $a$ are all 0.
\end{itemize}

You can also build your own divisibility test. If we can write $q$ as the product of two integers that are relatively prime and each has a known divisibility test, then an integer $a$ is divisible by $q$ if and only if $a$ passes the divisibility tests of each of the two integers mentioned earlier. In fact, Some of the divisibility test listed earlier employ such tactic. For example, when $q=12$, we can write 12 as $12=3\times 4$, where $\gcd(3, 4)=1$.

It is important to know that each divisibility test is an ``if and only if" statement. In other words, if a number passes the divisibility test for $q$, then the number is divisible by $q$; if the number is divisible by $q$, then the number will definitely pass the divisibility test for $q$.

\section{Examples}
\textbf{Example 1}\\
Given 45 digits, $\{1, 2, 2, 3, 3, 3, \cdots, 9\}$ such that there are $i$ digits $i$. A number is formed by using all of these 45 digits. Is it possible to form a number that is divisible by 9?\\
\textit{Solution}\\
Clearly, this problem requires the divisibility test of 9, which states that an integer is divisible by 9 if and only if the sum of digits of that number is divisible by 9. In the earlier chapter \textit{Tips and Tricks on Decimal Arithmetic}, we know that the sum of several terms is independent of the order in which the terms are arranged. Hence, the sum of digits for this 45-digit number is 
\[1\times1+2\times2+3\times3+4\times4+\cdots+9\times9 = 285\]
It is easy to see that 285 is not divisible by 9. Hence, it is impossible to use all the given digits and form an integer divisible by 9.\\
\textbf{Example 2}\\
Find all possible values of $a$ and $b$ such that $\overline{1a3b5}$ is divisible by 75.\\
\textit{Solution}\\
This problem suggests the use of divisibility test, but we do not have the divisibility test for 75. Hence, we need to construct our own divisibility test. It is clear that $\overline{1a3b5}$ is divisible by 75 if and only if $\overline{1a3b5}$ is divisible by 3 and 25. Therefore, we can use the divisibility tests for 3 and 25. In order for $\overline{1a3b5}$ to be divisible by 25, $\overline{b5}$ must be divisible by 25. Therefore, $b=2$ or $b=7$. Next, to fulfill the fact that $3|\overline{1a3b5}$, we need to have $3|(1+a+3+b+5)$ or $3|(9+a+b)$. When $b=2$, $3|(11+a)$. Since $1\le a\le 9$, hence $a=1$, $a=4$ or $a=7$. Similarly, when $b=7$, $3|(16+a)$, which leads us to $a=2$, $a=5$ or $a=8$. Hence, the solution set is
\[(a, b) \in \{(1, 2), (4, 2), (7, 2), (2, 7), (5, 7), (8, 7)\}\]
Comment: The trick of creating your own divisibility test and be further generalized. Suppose you wish to test for the divisibility test for $q$, and $q$ can be written as $q=\alpha_1\times\alpha_2\times\cdots\times\alpha_k$, for some positive integer $k$, such that $\alpha_1, \alpha_2,\cdots,\alpha_k$ are \emph{pairwise relatively prime}. In other words, $\gcd(\alpha_i, \alpha_j)=1$ for all $1\le i, j\le k, i\ne j$. In such situation, the test subject can pass all of the divisibility test for $\alpha_1, \alpha_2, \cdots, \alpha_k$ if and only if the test subject is divisible by $q$.\\
\textbf{Example 3}\\
Given a number $n$ defined as below:
\[n = \underbrace{20132013\cdots2013}_{k \mbox{ sets of } 2013}\]
Find the smallest positive integral value of $k$ such that $n$ is divisible by 11.\\
\textit{Solution}\\
Recall the divisibility test for 11, that is, a number is divisible by 11 if ans only if the alternate digit sum for that digit is divisible by 11. The alternate digit sum for $n$ is
\[\underbrace{(+2-0+1-3)+(+2-0+1-3)+\cdots+(+2-0+1-3)}_{k \mbox{ sets}}\]
But notice that $+2-0+1-3$ is 0, which is divisible by 11. Hence, $k$ can take any positive value and $n$ will still be divisible by 11. To minimize $k$, just take $k=1$ or $n=2013$.\\
\textbf{Example 4}\\
Given two positive integers $a$ and $b$, such that $a$ and $b$ can take any value as long as the digits of $a$ and $b$ consist of only 1, 2, 3 and 4. For example, 111222333444 can be used but 111111115 cannot be used. It is given that $a=29b$. Find an example of $a$ and $b$.\\
\textit{Solution}\\
You might be trying to find the divisibility test for 29, but, unfortunately, 29 is a prime number, hence, it is impossible to construct your own divisibility test using the method described earlier. Here, we shall employ some tactics from \textit{Periodic Function}, that is $d(ab)=d(d(a)d(b))$, where $d(n)$ denote the last digit of $n$, for some integer $n$. Since the last digit of $b$ can be 1, 2, 3 or 4 only. Take the product of 9 and these values, the last digit of $a$ can be 9, 8, 7 or 6 only. But the problem statement clearly states that $a$ can take 1, 2, 3 or 4 as its digits only. Hence, there does not exist such value of $a$ and $b$.

\section{Exercises}
\begin{enumerate}
\item Given a set of three consecutive two-digit numbers. It is known that the sum of these three numbers is divisible by 11. Find all such set.
\item Find an example of $n$ such that $n$ is divisible by 125 and the sum of digits of $n$ is 125.
\item Find an example of a pair of integers $a$, $b$ where $10\le a, b\le 99$ the all the digits of $c=ab$ are odd.
\item Let $n=\underbrace{111\cdots1}_{10}\underbrace{222\cdots2}_{10}\cdots\underbrace{999\cdots9}_{10}\Box$. Fill in the blank box so that $n$ is divisible by 11.
\item Let $n$ be the product of any 10-digit number and 123456789. Let $a_1$ be the sum of digits of $n$ and let $a_k$ be the sum of digits of $a_{k-1}$ for all $k>1$. Let $k$ be the smallest integer that satisfy $a_k=a_{k+1}$. Find $a_k$.
\item Pick four unique digits from $\{0, 1, 2, 3, \cdots, 8, 9\}$ to form a four-digit number that is divisible by 2, 3 and 5. What is the smallest and the largest of such number?
\item Given 2013 soldiers lining up in a row. At first, the soldiers were numbered from left to right using the numbers from 1 to 2013. Next, the soldiers were from right to left using numbers from 1 to 2013. After that, each soldier sum up the two numbers that were assigned to them. Find all integers $q$ such that $q$ divides the sum of the two numbers for each soldier. 
\item Given a four-digit number $\overline{3abc}$ such that this number is divisible by 15. How many such number are there?
\item Given that $k=\underbrace{1234 1234 1234 \cdots 1234}_{n \mbox{ sets of } 1234}$. What is the smallest value of $n$ such that $k$ is divisible by 11?
\item We define concatenate $a$ to $b$, where $a$ and $b$ are integers, as writing $a$ at the end of $b$ to form a new number. For example, concatenating 10 to 25 will produce 2510. Now, we say that a natural number $k$ is \textit{cute} when for any natural number $n$ and the number formed by concatenating $k$ to $n$ is divisible by $k$. For example, 1 is cute because for any natural number, say 1234, and concatenate 1 to 1234 to form 12341, which is divisible by 1. How many cute numbers less than 1000 are there?
\item We define a palindrome as a number that reads the same forward and backward. For example, 1, 121 and 1234321 are palindromes but 12 and 3445 are not. Give an example of a five-digit palindrome that is divisible by 11.
\item How many palindromes are there that has evenly many digits and is a prime? For example, 11 satisfies the problem statement.
\item There is a number $k$ that consists of \emph{only} the digit 2. It is known that this number is divisible by $1\underbrace{999\times9}_{2013}8$. How many digits are there for such $k$?
\item Given numbers from 1 to 150. Is it possible to arrange these numbers around a circle so that the sum of any 5 consecutive integers is always divisible by 3? Explain your answer.
\end{enumerate}

\section{Answers to Exercises}
\begin{enumerate}
\item $\{(10, 11, 12), (21, 22, 23), (32, 33, 34), (43, 44, 45), (54, 55, 56), (65, 66, 67), (76, 77, 78), (87, 88, 89)\}$
\item $\underbrace{999\cdots9}_{13}125$
\item $a=13$, $b=15$
\item 0
\item 9
\item Smallest = $1230$, largest = $9870$.
\item 1, 2, 19, 38, 53, 106, 1007 and 2014.
\item 67
\item 11
\item 12
\item 55055
\item 1
\item 18126
\item There is no such arrangement.
\end{enumerate}

\section{Solutions to Exercises}
\begin{enumerate}
\item Let the first of the three numbers be $a$. Clearly, we know that the three consecutive numbers are $a$, $a+1$, and $a+2$ while their sum is $a+(a+1)+(a+2)=3a+3=3(a+1)$. Since 3 and 11 are coprime, in order for $3(a+1)$ to be divisible by 11, $a+1$ must be divisible by 11, or $11|a+1$. Hence, we can say that $a+1=11n$, for some natural number $n$, or $a=11n-1$ for all natural number $n$ as long as $a\ge 10$ and $a+2 \le 99$. From these facts, it is easy to deduce that $n=1, 2, 3, \cdots, 8$, which gives us the solution set $\{(10, 11, 12), (21, 22, 23), (32, 33, 34), (43, 44, 45), (54, 55, 56), (65, 66, 67), (76, 77, 78), (87, 88, 89)\}$.
\item We can see that $125 = 5^3$. Hence, we can use the generalized divisibility test for $5^n$, for all natural number $n$. In other words, the number formed by the last three digits of $n$ is divisible by 125. For the sake of simplicity, we can let this number be 125. The digits in front do not affect the divisibility of $n$ by 125. Hence, we only need to ensure that the sum of digits is 125. There are many configurations that allow us to do so, and one of them is $\underbrace{999\cdots9}_{13}125$.
\item There are really \emph{many} such pairs and finding one of such pair by brute force is not difficult, but we shall deduce a logical method to do so. Let $a=10a_1+a_2$ and $b=10b_1+b_2$. It is easy to see that $a_1$ and $b_1$ are the first digits of $a$ and $b$, respectively, while $a_2$ and $b_2$ are the last digits of $a$ and $b$, respectively.
\[ab=(10a_1+a_2)(10b_1+b_2)=100(a_1b_1)+10(a_1b_2+a_2b_1)+a_2b_2\]
Now, to make life easier, we can \emph{try} to make $a_1b_1$ to be the first digit, $a_1b_2+a_2b_1$ to be the second digit and $a_2b_2$ to be the last digit of $c$, but this is impossible. To achieve this, $a_1b_1$, $a_1b_2+a_2b_1$ and $a_2b_2$ each must be at most 9 and odd. Hence, $a_1b_1$ is odd which implies $a_1$ and $a_2$ are also odd. Similarly, by analyzing the parity of $a_2b_2$, we can conclude that $a_2$ and $b_2$ are odd as well. This will forces $a_1b_2+a_2b_1$ to be even. Our alternative strategy is to ensure $a_1b_1 \le 9$, $a_1b_2+a_2b_1+1\le9$ and $10<a_2b_2<20$. For the sake of simplicity, let $a_1=b_1=1$, and to have $10<a_2b_2<20$ while ensuring $a_1b_2+a_2b_1=a_2+b_2+1\le9$, we need to have $a_2=3$ and $b_2=5$, or $a=12$, $b=15$. Notice that certainly there are other answers but the question requires only one example.
\item It is easy to see that the alternate digit sum of $n$ before the empty box is 0. Hence the only possible value that can be filled into the box is 0.
\item The question asks for the sum of digits of a (approximately) 19-digit number. This is a hint that the value of the product does not matter much. Notice that 123456789 is divisible by 9. Recall that when any number is multiplied by another number that is divisible by 9, the product is also divisible by 9, which implies that the sum of digits of the product is disivisible by 9. Repetitively taking the sum of digits (which is actually what $a_i$ means, will lead us to a single-digit number, which also is divisible by 9. Clearly, there are only one such number, that is, 9.
\item This problem is a direct application of the divisibility tests of 2, 3 and 5. For 2, the last digit must by even; for 5, the last digit must be either 0 or 5. To satisfy both conditions, the last digit must be 0. To ensure the number is divisible by 3, its digit sum must be divisible by 3. Hence, it is easy to see that the smallest such number is 1230 while the lasgest such number is 9870.
\item This problem might at first seems extremely difficult as we have to find \emph{all} common divisors of a set of 2013 numbers. For problems involving large number, it is often helpful to list out a few values and try to find a patter from them. Let $\{a_i\}_{i=1}^{2013}$ be the sequence of sum of the two numbers of each soldier from left to right. $a_1=1+2013=2014$, $a_2=2+2012=2014$, $a_3=3+2012=2014$. A very simple pattern now emerges, that is $a_1=a_2=a_3=\cdots+a_{2013}=2014$. Now, this pattern can only be considered a \emph{conjecture}. To use this fact, we have to \emph{prove} that it is true. Since the proof is fairly easy, it will be left as another exercise for the readers. The answer to this question is then reduced to finding the divisors of 2014, which are 1, 2, 19, 38, 53, 106, 1007 and 2014.
\item To have $\overline{3abc}$ to be divisible by 15, the digit sum of this number must be divisible by 3 and the its last digit must be either 0 or 5. We shall considerthe last digit first. When $c=0$, $3|a+b$, hence, it is easy to see that $a+b=0, 3, 6, 9, 12, 15, 18$. We can list down how many pairs of $(a, b)$ are there that satisfies the summation. There are 34 such pairs. Now, then $c=5$, we need to have $3|a+b+2$, or, in other words, $a+b=1,4,7,10,13,16$. Again, we count by listing down the possible pairs, which turns out to be 33. Hence, there is a total of 67 such numbers.
\item Clearly, we need to use the divisibility test for 11, which requires us to compute the alternate digit sum of $k$, which is
\[(1-2+3-4)+(1-2+3-4)+\cdots+(1-2+3-4)=(-2)+(-2)+(-2)+\cdots+(-2)=-2n\]
Now, we need to have $-2n$ to be divisible by 11, while minimizing $n$. You might not have encountered problems regarding the division of negative numbers, but the division of negative numbers is exactly the same as the division of positive numbers. (For now, you can imagine that the negative sign is not there.) Since 2 and 11 are coprime, $n$ need to be divisible by 11. The smallest such $n$ is clearly 11.
\item When we concatenate $a$ to $b$, we can write the number produced as $b\times10^k+a$, where $k$ is the number of digits of $a$. For example, when we concatenate 123 to 312, we can write the number produced as $312\times 10^3 + 123=312\times1000+123=312123$. Now, we can redifine \textit{cute} numbers: a natural number $a$ is cute if and only if for \emph{any} natural number $b$, $a|b\times10^k+a$, where $k$ is the number of digits of $a$. Since we know that $a|a$ for all natural number $a$, we can rewrite the last expression as $a|b\times10^k$. Since $b$ can by \emph{any} natural number, we can ensure that $a|b\times10^k$ by constructing $a$ such that $a|10^k$. This is much simpler than the original problem statement. All we need to do now is to plug in $k=1, 2, 3$ and count the number of $a$ that satisfies $a|10^k$. This turns out to be 12, and the list of values of $a$ is $\{1, 2, 5, 10, 20, 25, 50, 100, 125, 200, 250, 500\}$.
\item We can write a five-digit palindrome as $\overline{abcba}$. Applying the divisibility test of 11, we have $11|a-b+c-b+a$ or $11|2a-2b+c$. The easiest answer will be to let $2a-2b+c=0$. To further simplify things, let $a=b, c=0$, and we have $2a-2b+c=0$. In the answer provided, we choose $a=5, b=5, c=0$.
\item While you might have scratched your head for hours trying to find an example other than 11 (if you did so, well done for your perseverance!), there are really exactly 1 such number only. We shall prove this fact later on. Careful reader might notice that all palindrome with evenly many digits are divisible by 11. This is true, and will be the cornerstone of our proof. First of all, we should know that \emph{all} palindromes with evenly many digits can be written as $\overline{a_1a_2\cdots a_{n-2}a_{n-1}a_na_na_{n-1}a_{n-2}\cdots a_2a_1}$. Now, the secret lies on the alternate digit sum of such number. The alternate digit sum is exactly 0. For example, for $n=2$, the alternate digit sum of $\overline{a_1a_2a_2a_1}$ is $a_1-a_2+a_2-a_1=0$. This is true for any other $n$. Knowing that the alternate digit sum of any palindrome with evenly many digits is 0, we can conclude that all such palindromes are divisible by 11. Hence, 11 is the only palindromic prime with evenly many digits.
\item $1\underbrace{999\cdots9}_{2013}8$ might seems to be a huge number with no special characteristics, but it is actually a very interesting number. Let us investigate such number with less 9's. 
\[198=2\times9\times11\]
\[1998=2\times9\times111\]
\[19998=2\times9\times1111\]
\[\vdots\]
\[1\underbrace{999\cdots9}_{2013}8=2\times9\times\underbrace{111\cdots1}_{2014}\]
Now, to have $k$ divisible by such gigantic number, we need to have $k$ to be divisible by 2, 9 and $\underbrace{111\cdots1}_{2014}$. The divisibility of 2 is not an issue as the last digit of $k$, which consists of only the digit 2, is definitely even. The divisibility of 9 can be taken care of by ensuring the digit sum of $k$ is a multiple of 9. Since 2 and 9 are coprime, the number of digits of $k$ must also be divisible by 9. Notice that 
\[k=\underbrace{222\cdots2}_m=2\times\underbrace{111\cdots1}_m\]
where $m$ is the number of digits of $k$. Clearly 2 and $\underbrace{111\cdots1}_{2014}$ are relatively prime. Hence, $\underbrace{111\cdots1}_m$ is divisible by $\underbrace{111\cdots1}_{2012}$. It is clear that $k$ is must a multiple of 2012. But earlier on, we deduced that $k$ must also be a multiple of 9. Hence, $k$ can be the lowest common multiple of 9 and 2014, or 18126. Notice that $k$ can take any common multiple of 9 and 2014. Therefore, there are really more than one (in fact, infinitely many) answers to this problem.
\item We argue that there does not exist such arrangement. Suppose the contrary, that is, there is such arrangement. Pick any term in this arrangement and let this number be $a_1$. Let $a_n$ be the term to the right of $a_{n-1}$, for all $n>1$. (The perspective to see right is not important.) We know, by our assumption, that $3|a_1+a_2+a_3+a_4+a_5$ and $3|a_2+a_3+a_4+a_5+a_6$. We can see that $a_1$ has the same remainder as $a_6$ when divided by 3. Now, we define $x\equiv y$ when $x$ and $y$ has the same remainder when divided by 3. Hence, we can say that $a_1\equiv a_6$. By the similar method, we can conclude that when we \emph{partition} the numbers into group of 5s, the first terms in each partition have the same remainder, the second terms have the same remainder, etc. Hence, on the other perspective, the number of integers in each parition that have a remainder of $k$, when divided by 3 are the same. Therefore, let $a$, $b$ and $c$ be the numbers of integers that has remainder 0, 1 and 2, respectively, when divided by 3. Clearly, we have $a+b+c=5$. Since there are 30 such partition, so, there are $30a$, $30b$ and $30c$ numbers from 1 to 150 that has a remainder of 0, 1 and 2, respectively, when divided by 3. But notice that we can actually calculate the value of $30a$, $30b$ and $30c$. Their values turn out to be
\[30a=30b=30c=50\]
Clearly, there are no such natural numbers $a$, $b$ and $c$ that satisfy such condition.\\
Comment 1: This problem can be generalized to take integers from 1 to $5n$ for any natural number $n$.\\
Comment 2: In this problem, we employ a very useful tactic to prove a statement. The general procedure for this tatic is to first assume whatever that you want to prove is wrong, and then arrive at a statement that is completely absurd. That is why this method is called \textit{reductio ad absurdum}.
\end{enumerate}
\end{document}
